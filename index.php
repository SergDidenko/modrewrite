<?php
require 'bootstrap.php';
// define constants
define('SIZE', 50);
define('ANGLE', 0);
define('X', 160);
define('Y', 100);
define('RED', 100);
define('GREEN', 100);
define('BLUE', 100);
define('COUNTER', 0);
$url = explode('-', $_SERVER['REQUEST_URI']);
if ('/image' == $url[0]) {
  //header('content-type: image/png');
  if (isset($url[1]) && preg_match('/^[\d]+$/', $url[1])) {
    $id  = $url[1];
    $res = $con->prepare('SELECT * FROM test WHERE id=?');
    $res->execute([$id]);
    $res = $res->fetchAll(PDO::FETCH_OBJ);
    if (!empty($res)) {
      foreach ($res as $r) {
        $caption    = $r->image_title;
        $image_path = $r->image_path;
      }
      $s = $con->prepare('UPDATE test SET counter=counter+1 WHERE id=?');
      $s->execute([$id]);
//      $image = file_get_contents('templates/image-' . $id . '.html');
      //$image = $env->render('image.twig', ['image_path' => $image_path]);
      //$img   = [];
      //preg_match_all('/(src)=("[^"]*")/i', $image, $img);
      $image = imagecreatefrompng($image_path);
      $font  = 'fonts/a_Alterna.ttf';
      $color = imagecolorallocate($image, RED, GREEN, BLUE);
      if (isset($caption)) {
        imagettftext($image, SIZE, ANGLE, X, Y, $color, $font, $caption);
      }
      ob_start();
      imagepng($image);
      $raw = ob_get_contents();
      ob_end_clean();
      //imagedestroy($image);
      $img_encode = base64_encode($raw);
      echo $env->render('image.twig', ['img_encode' => $img_encode]);
    }else{
      header('Location:/');
    }
  }
}
elseif ('/' == $url[0]) {
  if (isset($_POST['caption']) && isset($_FILES['upload'])) {
    $errors  = [];
    $caption = $_POST['caption'];
    $file    = $_FILES['upload'];
    $name    = $file['name'];
    if (0 === $file['error'] && 'png' == explode('/', $file['type'])[1]) {
      move_uploaded_file($file['tmp_name'], 'images/' . $name);
      $new_path = 'images/' . $name;
      $s        = $con->prepare('INSERT INTO test(image_title, image_path, counter) VALUES(?, ?, ?)');
      $s->execute([$caption, $new_path, COUNTER]);
    }
    else {
      $errors[] = 'Error';
    }
  }
  echo $env->render('form.twig', ['errors' => $errors]);
}
