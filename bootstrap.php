<?php
require "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem('templates');
$env = new Twig_Environment($loader);
$con = new PDO('mysql: host=localhost; dbname=modrewrite', 'root', 'root', [PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING]);